package com.Education.server;

import com.Education.pojo.Schedule;

public interface ScheduleService {

    public void saveSchedule(Schedule schedule);

    public void removeSchedule(String id);

    public Schedule querySchedule(String id);

    public void updateSchedule(Schedule schedule);
}
