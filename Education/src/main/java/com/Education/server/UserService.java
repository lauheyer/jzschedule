package com.Education.server;

import com.Education.pojo.User;

import java.util.List;

public interface UserService {

    public User getUserByName(String username);

    public User getUser(String username,String password);

    public void addUser(User user);

    public void updateUserJseRou(User user);

    List<User> findAll();
}
