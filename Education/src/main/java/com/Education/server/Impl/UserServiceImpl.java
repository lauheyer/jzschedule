package com.Education.server.Impl;

import com.Education.dao.UserDao;
import com.Education.pojo.User;
import com.Education.server.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User getUserByName(String username) {
        return userDao.getUserByName(username);
    }

    @Override
    public User getUser(String username, String password) {
        return userDao.getUser(username,password);
    }

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public void updateUserJseRou(User user) {
        userDao.updateUserJseRou(user);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }
}
