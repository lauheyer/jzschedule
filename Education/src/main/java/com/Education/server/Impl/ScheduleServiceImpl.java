package com.Education.server.Impl;

import com.Education.dao.ScheduleDao;
import com.Education.pojo.Schedule;
import com.Education.server.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    private ScheduleDao scheduleDao;

    @Override
    public void saveSchedule(Schedule schedule) {
        scheduleDao.saveSchedule(schedule);
    }

    @Override
    public void removeSchedule(String id) {
        scheduleDao.removeSchedule(id);
    }

    @Override
    public Schedule querySchedule(String id) {
        return scheduleDao.querySchedule(id);
    }

    @Override
    public void updateSchedule(Schedule schedule) {
        scheduleDao.updateSchedule(schedule);
    }
}
