package com.Education.util;

import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpPostUtils {


    public HttpPost post(String url,List<NameValuePair> parameters) throws IOException {

        //声明post
        HttpPost httpPost = new HttpPost(url);

        //参数编码
        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(parameters,"UTF-8");

        //设置请求和传输超时时间
        //RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(2000).setConnectTimeout(2000).build();
        //构建超时等配置信息
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(10000) //建立socket链接超时时间
                .setConnectionRequestTimeout(10000) //从连接池中取的连接的最长时间
                .setSocketTimeout(10 *1000) //socket连接建立成功, 数据传输响应超时
                //.setStaleConnectionCheckEnabled(true) //提交请求前测试连接是否可用
                .build();

        //浏览器Agent
        httpPost.addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
        // 传输的类型
        //httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        httpPost.addHeader("Connection","keep-alive");
        httpPost.setEntity(formEntity);
        httpPost.setConfig(requestConfig);

        return httpPost;

    }

}
