package com.Education.util;

import java.util.ArrayList;
import java.util.List;

public class WeekUtils {

    public static List parseWeek(String weekdata){

        //切割类型：“1-4周,6-16周”，“3周,6-16周”，“3周”，“1-15周”，“1-15周(单)”，“1-4周,6-16周‘双’，“1-9周(单),13-15周(单)”
        //String str = "1-9周(单),13-15周(单)";
        String str = weekdata;

        List weeklist = new ArrayList();    //存放1-4周,6-16周中的周数
        String[] strs=str.split(",");   //如果有逗号，以逗号为切割点切割

        //切割后的数据类型：1-4周，3周，1-15周(单)，6-16周(双)
        for(int i=0,len=strs.length;i<len;i++){
            System.out.print(strs[i]+" ");

            //strs[i] = strs[i].substring(0, strs[i].length() -1);    //除去“周”字
            String remove = "周";
            strs[i] = strs[i].replace(remove,""); //除去“周”字

            //判断去周后是否为纯数字,比如：3
            if(!strs[i].contains("-")){
                System.out.println("不存在：'-'");
                weeklist.add(Integer.parseInt(strs[i]));
            }else if(strs[i].contains("单")){    //判断是否为单周

                System.out.println("存在：'单'");

                String singleweek = "(单)";
                strs[i] = strs[i].replace(singleweek,""); //除去“(单)”字

                String[] singleweeks=strs[i].split("-");

                //遍历切割后的字段，如：1 13，获取其中的周数，添加到集合中
                for(int singleStr = Integer.parseInt(singleweeks[0]); singleStr<=Integer.parseInt(singleweeks[1]); singleStr=singleStr+2){
                    System.out.print(singleStr+" ");
                    weeklist.add(singleStr);
                }
            }else if(strs[i].contains("双")){    //判断是否为双周

                System.out.println("存在：'双'");

                String doubleweek = "(双)";
                strs[i] = strs[i].replace(doubleweek,""); //除去“(双)”字

                String[] doubleweeks=strs[i].split("-");

                //遍历切割后的字段，如：1 13，获取其中的周数，添加到集合中
                for(int doubleStr = Integer.parseInt(doubleweeks[0]); doubleStr<=Integer.parseInt(doubleweeks[1]); doubleStr=doubleStr+2){
                    System.out.print(doubleStr+" ");
                    weeklist.add(doubleStr);
                }
            }else {
                String[] strs2=strs[i].split("-");  //把切割后的字段再以“-”切割
                //遍历切割后的字段，如：1 13，获取其中的周数，添加到集合中
                for(int j = Integer.parseInt(strs2[0]); j<=Integer.parseInt(strs2[1]); j++){
                    weeklist.add(j);
                }
            }

            System.out.println();

        }
        System.out.println("weeklist:"+weeklist);

        return weeklist;
    }

}
