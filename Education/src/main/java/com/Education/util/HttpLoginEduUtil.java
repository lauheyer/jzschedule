package com.Education.util;

import com.Education.pojo.User;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpLoginEduUtil {

    public static User loginEdu(User user) throws IOException {

        String username = user.getUsername();
        String password = user.getPassword();

        //若s数据库中用户不存在，则去教务网站尝试登录
        //获取连接客户端工具(创建httpClient实例)，(创建cookieStore实例)
        HttpClientSession httpClientSession = HttpClientUtils.getHttpClient(username);
        CloseableHttpClient httpClient = httpClientSession.getCloseableHttpClient();
        CookieStore cookieStore = httpClientSession.getCookieStore();

        String loginurl = "http://jw.jluzh.edu.cn/xtgl/login_slogin.html";
        //设置参数
        List<NameValuePair> parameters = new ArrayList<NameValuePair>(0);
        parameters.add(new BasicNameValuePair("yhm", username));
        parameters.add(new BasicNameValuePair("mm", password));

        //声明post请求
        HttpPostUtils httpPostUtils = new HttpPostUtils();
        HttpPost httpPost = httpPostUtils.post(loginurl,parameters);

        //5.发送请求，返回内容
        CloseableHttpResponse response = httpClient.execute(httpPost);

        int statusCode = response.getStatusLine().getStatusCode();

        //重定向后的地址
        if(response.getFirstHeader("Location")!= null){
            String edirectLocations = response.getFirstHeader("Location").getValue();
            System.out.println("edirectLocations:"+"http://jw.jluzh.edu.cn"+edirectLocations);
        }

        //StatusCode:200页面还是停留登录界面，302重定向学生信息界面
        if(statusCode == 302){
            if ("/xtgl/index_initMenu.html".equals(response.getFirstHeader("Location").getValue())){
                String JSESSIONID = null;
                String route = null;
                List<Cookie> cookies = cookieStore.getCookies();
                for (int i = 0; i < cookies.size(); i++) {
                    if (cookies.get(i).getName().equals("JSESSIONID")) {
                        Cookie c = cookies.get(i);
                        System.out.println(cookies.get(i));
                        JSESSIONID = cookies.get(i).getValue();
                    }
                    if (cookies.get(i).getName().equals("route")) {
                        Cookie c = cookies.get(i);
                        System.out.println(cookies.get(i));
                        route = cookies.get(i).getValue();
                    }
                }
                System.out.println("JSESSIONIDByjizhu:"+JSESSIONID);
                user.setJSESSIONID(JSESSIONID);
                user.setRoute(route);
                //userService.addUser(user);

                //关闭资源
                response.close();
                httpClient.close();

                return user;
            }

        }else {
            return null;
        }
        return null;
    }

}
