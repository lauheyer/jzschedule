package com.Education.dao.Impl;

import com.Education.dao.UserDao;
import com.Education.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;

import java.util.List;

import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public User getUserByName(String username) {
        return mongoTemplate.findOne(new Query(Criteria.where("username").is(username))
                , User.class);
    }

    @Override
    public User getUser(String username, String password) {
        return mongoTemplate.findOne(new Query(new Criteria().andOperator(Criteria.where("username").is(username),Criteria.where("password").is(password))),User.class);
    }

    @Override
    public void addUser(User user) {
        mongoTemplate.insert(user);
    }

    @Override
    public void updateUserJseRou(User user) {
        Update update = new Update();
        update.set("JSESSIONID",user.getJSESSIONID());
        update.set("route",user.getRoute());
        mongoTemplate.updateFirst(query(where("username").is(user.getUsername())), update,User.class);

    }

    @Override
    public List<User> findAll() {
        return mongoTemplate.findAll(User.class);
    }
}
