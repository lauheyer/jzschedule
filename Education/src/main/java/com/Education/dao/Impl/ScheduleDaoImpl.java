package com.Education.dao.Impl;

import com.Education.dao.ScheduleDao;
import com.Education.pojo.Schedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class ScheduleDaoImpl implements ScheduleDao {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveSchedule(Schedule schedule) {
        mongoTemplate.insert(schedule);
    }

    @Override
    public void removeSchedule(String id) {
        mongoTemplate.remove(new Query(where("id").is(id)),Schedule.class);
    }

    @Override
    public Schedule querySchedule(String id) {
        return mongoTemplate.findOne(new Query(where("id").is(id))
                , Schedule.class);
    }

    @Override
    public void updateSchedule(Schedule schedule) {

        Update update = new Update();
        update.set("scheduleMap",schedule.getScheduleMap());
        mongoTemplate.updateFirst(query(where("id").is(schedule.getId())), update,Schedule.class);
    }
}
