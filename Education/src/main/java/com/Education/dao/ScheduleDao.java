package com.Education.dao;

import com.Education.pojo.Schedule;

public interface ScheduleDao {

    public void saveSchedule(Schedule schedule);

    public void removeSchedule(String id);

    public Schedule querySchedule(String id);

    public void updateSchedule(Schedule schedule);



}
