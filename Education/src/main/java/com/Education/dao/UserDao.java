package com.Education.dao;

import com.Education.pojo.User;
//import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDao {

    public User getUserByName(String username);

    //public User getUser(@Param("username") String username, @Param("password") String password);
    public User getUser(String username, String password);

    public void addUser(User user);

    public void updateUserJseRou(User user);

    List<User> findAll();
}
