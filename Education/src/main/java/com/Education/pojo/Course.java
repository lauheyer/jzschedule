package com.Education.pojo;

import lombok.Data;

import java.util.List;

@Data
public class Course {

    //5-6节 5-6 1-15周(单) 计算机组成原理 3.5 王婧 副教授 三教阶梯I205 I205 1
    private String jc;
    private String jcs;
    private String zcd;
    private String kcmc;
    private String xf;
    private String xm;
    private String zcmc;
    private String cdmc;
    private String cd_id;
    private String xqj;

    //skjc代表第几节，skcd表示上课长度
    private Integer skjc;
    private Integer skcd;

    //上课的周数
    private List weeklist;

}
