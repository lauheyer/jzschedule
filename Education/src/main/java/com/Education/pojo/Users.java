package com.Education.pojo;

import lombok.Data;

import java.util.Map;

@Data
public class Users {

    private Integer id;
    private String username;
    private String password;
    private String JSESSIONID;
    private String route;

}
