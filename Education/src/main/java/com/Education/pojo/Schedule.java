package com.Education.pojo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document
@Data
public class Schedule {

    /**
     * 主键ID
     */
    @Id
    private String id;     //用学号年级学期命名，如：0417xxxx20173
    private Map<String, Object> scheduleMap;

}
