package com.myTest;

import com.Education.Application;
import com.Education.pojo.User;
import com.Education.server.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = Application.class)
@RunWith(SpringRunner.class)
public class MongodbTest2 {

    @Autowired
    UserService userService;

    @Test
    public void addUser(){
        User user = new User();
        user.setUsername("002");
        user.setPassword("efg");
        user.setJSESSIONID("sdfs3hgfh5hdfdsfngfh4yhg");
        user.setRoute("fg9g68dg4gf2v5j4gy2v6");
        userService.addUser(user);
    }

    @Test
    public void getUserByName(){
        User user = userService.getUserByName("002");
        System.out.println(user);
    }

    @Test
    public void getUser(){
        User user = userService.getUser("002","ejfg");
        System.out.println(user);
    }

    @Test
    public void updateUser(){
        User user = new User();
        user.setUsername("001");
        user.setPassword("errfg");
        user.setJSESSIONID("sdfs3hgfh5hdfdsfngfh4yhg");
        user.setRoute("fg9g68dg4gf2v5j4gy2v6");
        userService.updateUserJseRou(user);
    }

}
