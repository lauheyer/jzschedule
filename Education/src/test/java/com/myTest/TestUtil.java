package com.myTest;

import com.Education.pojo.User;
import com.Education.server.UserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/*
* 测试普通类调用service的方法
*
* */
@Component          //声明spring组件
public class TestUtil {

    @Autowired
    UserService userService;

    public static TestUtil testUtil;    //将自己作为静态私有变量引入，使其在springMvc初始化前就被创建

    @PostConstruct              //通过@PostConstruct和@PreDestroy方法，实现初始化和销毁bean之前的操作
    public void init(){
        testUtil = this;
    }

    @Test
    public void test(){
        User user = testUtil.userService.getUserByName("1234");
        //String JSESSIONID1 = "1111";
        System.out.println("Mysql====user:"+user);
    }

    @Test
    public void test2(){
        User user = testUtil.userService.getUser("12rd44","****");
        //String JSESSIONID1 = "1111";
        System.out.println("Mysql====user:"+user);
    }
}
