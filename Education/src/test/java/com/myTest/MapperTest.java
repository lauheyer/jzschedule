package com.myTest;

import com.Education.Application;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = Application.class)
//@MybatisTest    //缓存mybatsitest注解
@RunWith(SpringRunner.class)
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)    //这个是启用自己配置的数据元，不加则采用虚拟数据源
//@Rollback(false)    //这个是默认是回滚，不会commit入数据库，改成false 则commit
public class MapperTest {

//    @Autowired
//    UserMapper userMapper;
//
//    @Test
//    public void addUser(){
//        User user = new User();
//        user.setUsername("lhl");
//        user.setPassword("123");
//        userMapper.addUser(user);
//    }
//
//    @Test
//    public void getUserByName(){
//
//        User user = userMapper.getUserByName("lhl");
//        System.out.println("user:"+user.toString());
//
//    }
//
//    @Test
//    public void getUser(){
//        User user = userMapper.getUser("lhl","123");
//        System.out.println("user:"+user.toString());
//    }
//
//    @Test
//    public void findAll(){
//        List<User> list = userMapper.findAll();
//        System.out.println("list:"+list);
//    }
//
//    @Test
//    public void getCookie(){
//        String c =userMapper.getUserByName("lhl").getJSESSIONID();
//        System.out.println("cookie:"+c);
//    }
}
