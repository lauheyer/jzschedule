package com.myTest;

import com.Education.util.HttpPostUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
//import org.mybatis.spring.boot.test.autoconfigure.MybatisTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HttpTest {
    private final Log logger = LogFactory.getLog(HttpTest.class);

    @Test
    public void login() throws IOException {

        String username = "04172201";
        String password = "wg123456";

        //获取连接客户端工具(创建httpClient实例)，(创建cookieStore实例)
        CookieStore cookieStore = new BasicCookieStore();
        CloseableHttpClient httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

        String loginurl = "http://jw.jluzh.edu.cn/xtgl/login_slogin.html";
        //设置参数
        List<NameValuePair> parameters = new ArrayList<NameValuePair>(0);
        parameters.add(new BasicNameValuePair("yhm", username));
        parameters.add(new BasicNameValuePair("mm", password));

        //声明post请求
        HttpPostUtils httpPostUtils = new HttpPostUtils();
        HttpPost httpPost = httpPostUtils.post(loginurl,parameters);

        //5.发送请求，返回内容
        CloseableHttpResponse response = httpClient.execute(httpPost);

        //logger.info("StatusCode:"+response.getStatusLine().getStatusCode());
        //logger.info("response1:"+response);

        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("statusCode2:"+statusCode);
        System.out.println("response:"+response);

        String JSESSIONID = null;
        String route = null;
        List<Cookie> cookies = cookieStore.getCookies();
        for (int i = 0; i < cookies.size(); i++) {

            System.out.println("cookies.get(" + i + ")================:"+cookies.get(i));

            if (cookies.get(i).getName().equals("JSESSIONID")) {
                Cookie c = cookies.get(i);
                System.out.println(cookies.get(i));
                JSESSIONID = cookies.get(i).getValue();
            }
            if (cookies.get(i).getName().equals("route")) {
                route = cookies.get(i).getValue();
            }
        }
        System.out.println("cookies:"+cookies);
        System.out.println("JSESSIONID:"+JSESSIONID);
        System.out.println("route:"+route);
        System.out.println("Location:"+response.getFirstHeader("Location").getValue());

    }


    @Test
    public void getMenu() throws IOException {

        //String username = "04172201";
        //String password = "wg123456";

        //获取连接客户端工具(创建httpClient实例)，(创建cookieStore实例)
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie1 = new BasicClientCookie("JSESSIONID", "7FFF0BEF12BF183C58EA883F431E2D1A");
        cookie1.setDomain("jw.jluzh.edu.cn");
        cookie1.setPath("/");
        cookieStore.addCookie(cookie1);
        BasicClientCookie cookie2 = new BasicClientCookie("route", "a391fe36246cf8f8b5e3f1a145408ef0");
        cookie2.setDomain("jw.jluzh.edu.cn");
        cookie2.setPath("/");
        cookieStore.addCookie(cookie2);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();

        System.out.println("自定义cookieStore:"+cookieStore);

        String loginurl = "http://jw.jluzh.edu.cn/xtgl/index_initMenu.html";
        //设置参数
        List<NameValuePair> parameters = new ArrayList<NameValuePair>(0);
        //parameters.add(new BasicNameValuePair("yhm", username));
        //parameters.add(new BasicNameValuePair("mm", password));

        //声明post请求
        HttpPostUtils httpPostUtils = new HttpPostUtils();
        HttpPost httpPost = httpPostUtils.post(loginurl,parameters);

        //5.发送请求，返回内容
        CloseableHttpResponse response = httpClient.execute(httpPost);

        //logger.info("StatusCode:"+response.getStatusLine().getStatusCode());
        //logger.info("response1:"+response);

        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("statusCode2:"+statusCode);
        System.out.println("response:"+response);

        HttpEntity entity2 = response.getEntity();
        String result = EntityUtils.toString(entity2, "utf-8");
        System.out.println("entity:"+result);

        //System.out.println("Location:"+response.getFirstHeader("Location").getValue());
    }


    @Test
    public void getCourse() throws IOException {

        //获取连接客户端工具(创建httpClient实例)，(创建cookieStore实例)
        CookieStore cookieStore = new BasicCookieStore();
        BasicClientCookie cookie1 = new BasicClientCookie("JSESSIONID", "3CCE5FB99DDF8A4D57E2EC864233FC07");
        cookie1.setDomain("jw.jluzh.edu.cn");
        cookie1.setPath("/");
        cookieStore.addCookie(cookie1);
        BasicClientCookie cookie2 = new BasicClientCookie("route", "442fe39b9b4d67d8e39e0d87e0a8af10");
        cookie2.setDomain("jw.jluzh.edu.cn");
        cookie2.setPath("/");
        cookieStore.addCookie(cookie2);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();

        System.out.println("自定义cookieStore:"+cookieStore);

        String loginurl = "http://jw.jluzh.edu.cn/kbcx/xskbcx_cxXsKb.html?gnmkdm=N2151";
        //设置参数
        //3.设置参数
        List<NameValuePair> parameters3 = new ArrayList<NameValuePair>(0);
        parameters3.add(new BasicNameValuePair("xnm", "2018"));
        parameters3.add(new BasicNameValuePair("xqm", "12"));

        //声明post请求
        HttpPostUtils httpPostUtils = new HttpPostUtils();
        HttpPost httpPost = httpPostUtils.post(loginurl,parameters3);

        //5.发送请求，返回内容
        CloseableHttpResponse response = httpClient.execute(httpPost);

        //logger.info("StatusCode:"+response.getStatusLine().getStatusCode());
        //logger.info("response1:"+response);

        int statusCode = response.getStatusLine().getStatusCode();
        System.out.println("statusCode2:"+statusCode);
        System.out.println("response:"+response);

        HttpEntity entity3 = response.getEntity();
        String result = EntityUtils.toString(entity3, "utf-8");
        System.out.println("entity:"+result);
        JSONObject jsonObject = JSONObject.parseObject(result);
        System.out.println("1"+jsonObject.getString("kbList"));

        //json格式化输出
        //ObjectMapper mapper = new ObjectMapper();
        //System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject));
    }

}
