// pages/course/course.js
var utils = require('../../utils/util.js');
var api = require('../../config/api.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    ischeck: false,
    firstYear: null, //入学年份
    showWeek: 1,
    weekindex: null,
    showSemester: 1,
    semesterindex: null,
    grade: '2017',
    semester: '3',
    picker: ['第1周', '第2周', '第3周', '第4周', '第5周', '第6周', '第7周', '第8周', '第9周', '第10周', '第11周', '第12周', '第13周', '第14周', '第15周', '第16周', '第17周', '第18周'],
    semesterList: ['大一 上', '大一 下', '大二 上', '大二 下', '大三 上', '大三 下', '大四 上', '大四 下'],
    colorArrays: ["#85B8CF", "#90C652", "#D8AA5A", "#FC9F9D", "#0A9A84", "#61BC69", "#12AEF3", "#e89abe", "#E29AAD"],
    //xqj代表星期几上课，skjc代表第几节，skcd表示上课长度，如2代表连上2节，kcmc代表具体内容。
    //wlist: [{ "xqj": 1, "skjc": 1, "skcd": 3, "kcmc": "高等数学@教A-301" }]
    courselist: null
  },

  WeekChange(e) {
    console.log(parseInt(e.detail.value) + 1);
    this.setData({
      //semesterindex: e.detail.value,
      showWeek: parseInt(e.detail.value) + 1
    })
    wx.setStorageSync('showWeek', parseInt(e.detail.value) + 1)
  },

  GradeSemesterChange(e) {
    let index = parseInt(e.detail.value)
    console.log(index);
    console.log('/' + parseInt(index / 2));
    console.log('%' + index % 2);

    this.setData({
      semesterindex: e.detail.value,
      showSemester: parseInt(e.detail.value),
      grade: parseInt(this.data.firstYear) + parseInt(index / 2) + ''
    })

    if ((index % 2) === 0) {
      this.setData({
        semester: '3',
      })
    } else {
      this.setData({
        semester: '12',
      })
    }

    wx.setStorageSync('showSemester', parseInt(e.detail.value))
    wx.setStorageSync('grade', this.data.grade)
    wx.setStorageSync('semester', this.data.semester)

    this.getSchedule(this.data.grade, this.data.semester)

  },

  showCardView: function(e) {
    console.log(e.currentTarget.dataset.index)
    var index = e.currentTarget.dataset.index
    console.log(this.data.courselist[index])
  },

  getSchedule: function(grade, semester) {
    let that = this
    let showgrade = grade
    let showsemester = semester
    console.log('showgrade:' + showgrade + ',showsemester:' + showsemester)
    wx.showNavigationBarLoading()
    utils.request(api.GetSchedule, {
        grade: showgrade,
        semester: showsemester
      }, "Get")
      .then(function(res) {
        wx.hideNavigationBarLoading()
        console.log('getSchedule:', res);
        if (res.errno === 0) {

          that.setData({
            courselist: res.data.courseList
          })

        }

      }, function(err) {
        wx.hideNavigationBarLoading()
        utils.showModal('获取失败', '请重新获取')

      });
  },

  //校验token
  checkToken: function() {
    let that = this
    wx.showNavigationBarLoading()

    if (wx.getStorageSync('token') == "") {
      wx.redirectTo({
        url: '../login/login',
      })
    } else {
      utils.request(api.CheckToken, "Get")
        .then(function(res) {
          wx.hideNavigationBarLoading()
          console.log('CheckToken:', res);
          if (res.errno === 0) {
            that.setData({
              ischeck: true
            })
          } else {
            utils.showModal('刷新失败', '请重新登录')
            wx.redirectTo({
              url: '/pages/login/login',
            })
          }

        }, function(err) {
          wx.hideNavigationBarLoading()
          utils.showModal('信息过期', '请重新登录')
          wx.redirectTo({
            url: '../login/login',
          })
        });
    }
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.checkToken()
    let thisWeek = wx.getStorageSync('showWeek')
    let thisSemester = wx.getStorageSync('showSemester')
    let firstyear = wx.getStorageSync('firstYear')
    this.setData({
      firstYear: firstyear
    })

    if (thisWeek == '') {
      this.setData({
        showWeek: 1
      })
    } else {
      this.setData({
        showWeek: thisWeek
      })
    }
    if (thisSemester == '') {
      this.setData({
        showSemester: 0
      })
    } else {
      this.setData({
        showSemester: thisSemester
      })
    }

    let pushgrade = wx.getStorageSync('grade')
    let pushsemester = wx.getStorageSync('semester')

    if (pushgrade == '' || pushsemester == '') {
      pushgrade = parseInt(firstyear) + parseInt(thisSemester / 2) + ''
      if ((thisSemester % 2) === 0) {
        pushsemester = '3'
      } else {
        pushsemester = '12'
      }
    }

    this.getSchedule(pushgrade, pushsemester)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})