// pages/login/login.js
var utils = require('../../utils/util.js');
var api = require('../../config/api.js');
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    login: true,
  },


  formSubmit: function(e) {
    let that = this
    let username = e.detail.value.username
    let password = e.detail.value.password
    if (username && password) {
      console.log("username:" + username + ";password:" + password)
      wx.showNavigationBarLoading()

      // utils.request(api.Login, {
      //   username: username,
      //   password: password
      // }, "POST").then(function(res) {
      //   console.log('login:', res);
      //   if (res.errno === 0) {
      //     wx.setStorageSync('Authorization', res.data.token)
      //     wx.redirectTo({
      //       url: '../course/course',
      //     })
      //   } else {
      //     utils.showModal('登录失败', result.data.message)
      //   }

      // }, function(err) {
      //   console.log('login:', res);
      //   utils.showModal('登录失败', '请重试')
      // });


      wx.request({
        url: api.Login,
        method: 'POST',
        data: {
          username: username,
          password: password
        },
        header: {
          'content-type': 'application/json'
        },
        success: function(res) {
          console.log("res:", res)
          console.log("token:", res.data.data.token)
          wx.setStorageSync('token', res.data.data.token)
          wx.setStorageSync('firstYear', res.data.data.firstYear)
          var tk = wx.getStorageSync('token')
          console.log("tk:", tk)
          app.globalData.studentID = username
          wx.switchTab({
            url: '/pages/course/course',
          })
          //resolve(res)
        },
        fail: function (err) {
          utils.showNoActionModal('登录失败', '登录失败')
        }
      })

    } else {
      utils.showNoActionModal('请填写教务系统信息', '教务系统账号和密码不能为空')
    }
  },

  //校验token
  checkToken: function() {
    let that = this
    wx.showNavigationBarLoading()

    if (wx.getStorageSync('token') == "") {
      that.setData({
        login: true
      })
    } else {
      utils.request(api.CheckToken, "Get")
        .then(function(res) {
          wx.hideNavigationBarLoading()
          console.log('login:', res);
          if (res.errno === 0) {
            wx.redirectTo({
              url: '../course/course',
            })
          }

        }, function(err) {
          wx.hideNavigationBarLoading()
          utils.showModal('信息过期', '请重新登录')
          that.setData({
            login: true
          })
        });
    }


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    //this.checkToken()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})