// 以下是业务服务器API地址
// 本机开发时使用
//var WxApiRoot = 'http://localhost:8080/wx/';
var WxApiRoot = 'http://127.0.0.1:8080/wx/';

module.exports = {
  Login: WxApiRoot + 'userlogin', //登录接口
  CheckToken: WxApiRoot + 'checkToken', //检验token,
  GetSchedule: WxApiRoot + 'getSchedule',  //获取课程
  Logout: WxApiRoot + 'logout',  //退出
};
