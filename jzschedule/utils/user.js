/**
 * 用户相关服务
 */
const util = require('../utils/util.js');
const api = require('../config/api.js');
var app = getApp();

/**
 * Promise封装wx.checkSession，异步查询
 * 有时候，我们会因为session_key不正确而导致解密或者校验签名失败。
 * 因为wx.login()被调用时，用户的session_key会被更新导致就session_key失效。所以，在调用wx.login()的时候应该要明确需要登录之后再调用。
 * 因为wx.login()被调用时，用户的session_key会被更新导致就session_key失效。所以，在调用wx.login()的时候应该要明确需要登录之后再调用。
 * 因此，就需要调用wx.checkSession()来校验当前用户的session_key是否有效。
 */
function checkSession() {
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        resolve(true);
      },
      fail: function () {
        reject(false);
      }
    })
  });
}

/**
 * Promise封装wx.login
 */
function login() {
  /**
   * promise代表一个异步操作的执行返回状态，这个执行返回状态在promise对象创建时未必已知。
   * 它允许你为异步操作的成功或失败指定处理方法。
   * 传入两个参数：resolve,reject，分别表示异步操作执行成功后的回调函数和异步操作执行失败后的回调函数
   */
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          console.log('ok:', res)
          resolve(res);
        } else {
          console.log('fail:')
          reject(res);
        }
      },
      fail: function (err) {
        console.log('??err:', err)
        reject(err);
      }
    });
  });
}

/**
 * 调用微信登录
 */
function loginByWeixin(e) {

  return new Promise(function (resolve, reject) {
    return login().then((res) => {

      //登录远程服务器
      console.log('userInfo:', e.detail.userInfo)
      console.log('rawData:',e.detail.rawData);
      console.log('signature:', e.detail.signature);

      util.request(api.AuthLoginByWeixin, {
        code: res.code,
        userInfo: e.detail.userInfo
      }, 'POST').then(res => {
        if (res.errno === 0) {
          
          //返回数据
          console.log('loginres:', res);
          console.log('返回数据userId:', res.data.userId);
          //获取数据库的用户id
          getApp().globalData.userId = res.data.userId;
          console.log('userId:', getApp().globalData.userId);

          //存储用户信息
          wx.setStorageSync('userInfo', res.data.userInfo);
          wx.setStorageSync('token', res.data.token);

          resolve(res);
        } else {
          console.log('微信登录失败')  //微信登录失败
          reject(res);
        }
      }).catch((err) => {
        console.log('err2:')
        reject(err);
      });
    }).catch((err) => {
      console.log('err3:')
      reject(err);
    })
  });
}

/**
 * 判断用户是否登录
 */
function checkLogin() {
  console.log('微信登录判断')
  return new Promise(function (resolve, reject) {
    console.log('微信登录判断2')
    if (wx.getStorageSync('userInfo') && wx.getStorageSync('token')) {
      checkSession().then(() => {
        console.log('微信登录了')
        //resolve('成功');  //状态由等待变为成功，传的参数作为then函数中成功函数的实参
        resolve(true);
      }).catch(() => {
        console.log('微信不登录')
        reject(false);
      });
    } else {
      console.log('微信不登录2')
      //reject('失败')  //状态由等待变为失败，传的参数作为then函数中失败函数的实参
      reject(false);
    }
  });
}

module.exports = {
  loginByWeixin,
  checkLogin,
};