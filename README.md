# JZSchedule

## 介绍
吉猪课程表是基于微信小程序端的、本校大学生的课程信息查询系统，学生能对自己的课程进行某学期
课程查询、刷新、和某节课程信息展示等。


## 涉及技术
- 运行环境：ecipse/idea,jdk1.8,MongoDB，微信开发者工具
- 后端框架：Springboot+MongoDB+Shiro+JWT+HtpClient+jsoup
- 微信小程序框架：colorUI

##  功能概述

学生能对自己的课程进行某学期课程查询、刷新、和某节课程信息展示等

更多功能开发中...



##  项目展示

### 项目结构

![](README.assets/1.png)

![](README.assets/2.png)



### 用户登录

![](README.assets/a.png)



### 课程表信息

![](README.assets/b.png)



### 课程表年级更换

![](README.assets/c.png)



![](README.assets/d.png)



### 课程表周期更换

![](README.assets/e.png)

![](README.assets/f.png)

### 个人中心

![](README.assets/g.png)









#### 参与贡献



1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
